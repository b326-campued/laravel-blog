<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Access the authenticated user via the Auth facade
use Illuminate\Support\Facades\Auth;

// Import model module to access database columns
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;



class PostController extends Controller
{   
    // Controller for user log out
    //Function for logging out of user
    public function logout(){
        // Logout the currently logged in user.
        Auth::logout();

        return redirect('login');

    }

    // Controller action to return view containing a form for a blogpost creation
    public function createPost(){
        return view('posts.create');
    }

    public function savePost(Request $request){
       // To check whether there is authenticated user or none
        if(Auth::user()){
            // Post here came from Model Post. In this we instantiate a new Post Object from the Post method then save it in $post variable.

            $post = new Post;

            // define the properties of the $post object using the received form data
            // title/content of $post object => objects properties that represent columns in tables
            // title/content in input => name attribute in create.blade.php of forms
            $post->title=$request->input('title');
            $post->content=$request->input('content');

            // Auth::user is an object
            // This wiill get the id of the authenticated user and set it as the foreign key user_id of the new post.
            $post->user_id=(Auth::user()->id);

            // Save post in our Post Table
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    // Controller that will return all the blog post
    public function showPosts(){
        // This method will bind all the records in our Post Table in our $post variable
        $posts = Post::all();

        // Pass $post to view
        return view('posts.showPosts')->with('posts', $posts);

    }

    // Action for showing only the posts authored by the authenticated user
    public function myPosts(){
        if(Auth::user()){
            $posts = Auth::user()->posts;

            return view('posts.showPosts')->with('posts', $posts);
        }else{
            return redirect('/login');
        } 
    }

    // Action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown
    public function showPost($id){

        $post = Post::find($id);

        return view('posts.showPost')->with('post', $post);
    }

    public function editPost($id){

        $post = Post::find($id);

        return view('posts.editPost')->with('post', $post);
    }

     public function updatePost($id, Request $request){

        $post = Post::find($id);

        $post->title=$request->input('title');
        $post->content=$request->input('content');
        $post->save();

        return view('posts.showPost')->with('post', $post);

    }


    public function archivePost($id){
        $post = Post::find($id);

        $post->isActive = false;
        $post->save();

        
        return redirect('/posts');
    }

    // $id here is id of post we are going to like in post table
    public function likePost($id){
        $post = Post::find($id);

        if($post->user->id != Auth::user()->id){
            // Unlike/Delete the like record made by user before
            if($post->likes->contains("user_id", Auth::user()->id)) {
                PostLike::where('post_id', $post->id)->where('user_id', Auth::user()->id)->delete();
            }else{
                // Create a new like record to like this post
                // Instantiate a new PostLike object from the PostLike model
                $postlike = new PostLike;
                // Define properties of $postlike
                $postlike->post_id = $post->id;
                $postlike->user_id = Auth::user()->id;


                // save $postlike object
                $postlike->save();

            }
        }

        return redirect("/posts/$id");
    }

    public function commentPost($id, Request $request){
        $post = Post::find($id);

        // Create a new like record to like this post
        // Instantiate a new comment object from the PostComment model
        $comment = new PostComment;
        // Define properties of $postlike
        $comment->content=$request->input('comment');
        $comment->post_id=$post->id;
        $comment->user_id=Auth::user()->id;

        // save $postComment object
        $comment->save();

        return redirect("/posts/$id");
    }
}
