<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{   

    // define a one to many relationship between the User model and Post model
    // user is singular as one to many relationship with post
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    // One to many relationship
    public function likes(){
        return $this->hasMany('App\Models\PostLike');
    }

    // One to many relationship
    public function comments(){
        return $this->hasMany('App\Models\PostComment');
    }
}
