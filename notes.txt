--Creating project in laravel
composer create-project laravel/laravel <project-name>

-- Used to run the server/run laravel app
php artisan serve

-- To install laravel's laravel/ui package via terminal command
* composer is package manager of php
composer require laravel/ui
** The build tje authentication scaffolding via the terminal command:
php artisan ui bootstap --auth 
npm install && npm run dev

-- app.blade.php (resources > views > layouts)
Where you can insert bootstrap cdn links


App Folder - contains the models at its root. Model represents our database entities and have predefined methods for querying the respective tables that they represent.

	*HTTP folder - Controllers subdirectory contains the project's controllers where we define our application logiv

database folder - migrations subdirectory contains the migrations that we will use to define the structures and datatypes of our database tables

public folder - contains the files that are directly accesssible to users. Where asset such as css, js, images etc can be stored and accessed.

resouces folder - views subdirectory is the namespace where all views will be looked for by our application

routes folder - web.php  is where we define the routes of our application.

-- To create a model
-- -mc means  
php artisan make:model name_model -mc
php artisan make:model name_model

--To create a new migration/migration file
php artisan make:migration table_name
---->php artisan make:migration create_post_likes_table


--DB connection is in .env

-- To migrate (initial)
php artisan migrate
* make sure you save changes in .env DB_DATABASE=database_name
* If we want to migrate you need to delete all contents of tables to take effect the migration
** if there is changes in model
php artisan migrate:fresh


-- Set relationships of models
** See Users
public function posts(){
        return $this->hasMany('App\Models\Post')
}

** See Post
public function user(){
        return $this->belongsTo('App\Models\User');
}

-- Controller, Routes , Views

ROUTES
assigns a functionality to particular UIR endpoint ex. return to hom view when GET request is received at /home

CONTROLLERS
Organize the request handling logic of our application ex. PostController

VIEWS 
Contains the presentation logic - Where HTML pages is saved

Blade
Templating engine for PHP

AUTHORIZATION 
Determines whether an authenticated user is permitted to perform action or not


STEPS
Add route in web.php
Add function logic in Controllers used in routes
Add view for the UI of routes and controllers

URL PARAMETERS and SELECTIVE QUERYING
-Url parameters are enclosed in {} 


--Associative Table (Comment and Like in Blog Post)
Commonly use to RDS 

