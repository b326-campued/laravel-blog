<?php

use Illuminate\Support\Facades\Route;

// Import controller module to be able to use by route
use App\Http\Controllers\PostController;

// Import model module to access database columns
use App\Models\Post;

use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    // This method will bind all the records in our Post Table in our $post variable
    $posts = Post::where('isActive', true)->inRandomOrder()->take(3)->get();

    return view('welcome')->with('posts', $posts);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// This route is for logging out
Route::get('/logout', [PostController::class, 'logout']);

// This route is for creation of new post
Route::get('/posts/create', [PostController::class, 'createPost']);

// This route is saving the post on our database
Route::post('/posts', [PostController::class, 'savePost']);

// This route is for the list of post on our databases
Route::get('/posts', [PostController::class, 'showPosts']);

// Define route that wil return a view containing only the authenticated users post
Route::get('/myPosts', [PostController::class, 'myPosts']);

// Defined a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user
// Dynamic endpoint id
Route::get('/posts/{id}', [PostController::class, 'showPost']);

Route::get('/posts/{id}/edit', [PostController::class, 'editPost']);

Route::put('/posts/{id}', [PostController::class, 'updatePost']);

Route::put('/posts/{id}/archive', [PostController::class, 'archivePost']);

// Define a website that will call function for liking and unliking specific posts
Route::put('/posts/{id}/like', [PostController::class, 'likePost']);

// Commenting in a blog post route
Route::post('/posts/{id}/comment', [PostController::class, 'commentPost']);

