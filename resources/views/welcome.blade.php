<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        

        <style>
            .laravel {
                display: block;
                max-height: 500px;
                max-width: 500px;
                margin-left: auto;
                margin-right: auto;
            }

            .card-title {
                font-size: 1.5rem;
            }
        </style>
    </head>
    <body>
            @extends('layouts.app')
            
            

            @section('content')
                <img class="img-fluid laravel mb-5" src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png">
                <h2 class="text-center mb-3">Featured Posts:</h2>
                
                @if($posts->isEmpty())

                    <h4 class="text-center">No active posts available.</h4>

                @else    

                    @if(isset($posts[0]))
                        <div class="card text-center col-10 mx-auto mt-2">
                            
                            <a class="card-title mb-3" href="/posts/{{$posts[0]->id}}">{{$posts[0]->title}}</a>
                            <p class="card-text mb-3">Author: {{$posts[0]->user->name}}</p>
                        </div>
                    @endif

                    @if(isset($posts[1]))
                        <div class="card text-center col-10 mx-auto mt-2">
                            

                            <a class="card-title mb-3" href="/posts/{{$posts[1]->id}}">{{$posts[1]->title}}</a>
                            <p class="card-text mb-3">Author: {{$posts[1]->user->name}}</p>
                        </div>
                    @endif
                    
                    @if(isset($posts[2]))
                        <div class="card text-center col-10 mx-auto mt-2">
                           
                            <a class="card-title mb-3" href="/posts/{{$posts[2]->id}}">{{$posts[2]->title}}</a>
                            <p class="card-text mb-3">Author: {{$posts[2]->user->name}}</p>
                        </div>
                    @endif
                @endif
            @endsection
    </body>
</html>
