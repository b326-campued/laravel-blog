@extends('layouts.app')

@section('tabName')
	{{$post->title}}
@endsection
@section('content')

	<div class="card col-8 mx-auto">	
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<h4>Content:</h4>
			<p class="card-text">{{$post->content}}</p>

			<p>Likes: {{count($post->likes)}} | Comments: {{count($post->comments)}}</p>

			<!-- <h1>{{$post->likes->where('user_id', Auth::id())}}</h1> -->

			<!-- Logged user is not poster of blog -->
			
			@if(Auth::user())
				@if(Auth::id() != $post->user_id)


				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					@method('PUT')
					@csrf
					<!-- If the user already liked the post or not -->
					@if($post->likes->contains('user_id', Auth::id()))
						<button class="btn btn-danger">Unlike</button>
					@else
						<button class="btn btn-success">Like</button>
					@endif

				</form>


				@endif

				<form class="d-inline">
						<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
						  Comment
						</button>
				</form>

			@endif
			<br/>
			<br/>


			<a href="/posts" class="card-link">View all post</a>
		</div>
	</div>

	<!-- COMMENTS -->
	@if($post->comments->isNotEmpty())

		<h4 class="col-8 mx-auto mt-5">Comments:</h4>

		@foreach($post->comments as $comment)

			<div class="card col-8 mx-auto mb-3">
			  <div class="card-body">
			    <h4 class="card-title text-center">{{$comment->content}}</h4>
			    <h5 class="card-subtitle text-muted mb-1 text-end">Posted by: {{$comment->user->name}}</h5>
				<p class="card-subtitle text-muted mb-3 text-end">Posted on: {{$comment->created_at}}</p>
			  </div>
			</div>

		@endforeach
	



	@endif
	<!-- END COMMENTS -->

	<!-- HTML MODAL START-->
	<form method="POST" action="/posts/{{$post->id}}/comment">
	@csrf
		<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h1 class="modal-title fs-5" id="exampleModalLabel">Leave Comment</h1>
		        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		      </div>
		      <h6 class="modal-subtitle ps-3 pt-2">Comment:</h6>
		      <div class="modal-body">
		        <textarea row = 3 style="width:100%" name="comment"></textarea>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
		        	<button type="submit" class="btn btn-primary">Comment</button>
		      </div>
		    </div>
		  </div>
		</div>
	</form>
	<!-- HTML MODAL END -->

@endsection