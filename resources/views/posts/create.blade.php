<!-- Extends app.blade.php. This directive is used at the beginning of a Blade template to specify that the current template (create.blade.php) should inherit the structure and content of another template named layouts.app. -->
@extends('layouts.app')

@section('tabName')
    Create Post
@endsection

<!-- More like DOM manipulation concept -->
@section('content')
		<form class="col-3 bg-secondary p-5 mx-auto" method="POST" action="/posts">
			
			<!-- CSRF stands for Cross-Site Request Forgery. It is a form of attack where malicious users may send malicious request while pretending to be authorized user -->

			@csrf
			<div class="form-group">
				<label for="title">Title:</label>
				<input type="text" name="title" class="form-control" id="title"/>
			</div>

			<div class="form-group">
				<label for="content">Content:</label>
				<textarea name="content" class="form-control" id="content" row = 3></textarea>
			</div>

			<div class="mt-2">
				<button class="btn btn-primary" type="submit">Create Post</button>
			</div>

		</form>
@endsection

	