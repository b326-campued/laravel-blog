@extends('layouts.app')

@section('content')

	<div class="col-8 mx-auto">	
			<form action="/posts/{{$post->id}}" method="POST">

				@method('PUT')
				@csrf

				<label for="title">Title:</label>
				<input type="text" style="width:100%" name="title" value="{{$post->title}}">
				<label for="content">Content:</label>
				<textarea row = 3 style="width:100%" name="content">{{$post->content}}</textarea>
				<button type="submit" href="" class="btn btn-primary">Update Post</button>
			</form>
	</div>

@endsection